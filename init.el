;;; init --- Summary
;;; Commentary:
;;; Code:

;; requires
(require 'package)
(require 'gnutls)
(require 'use-package-ensure)

;;; DEFAULTS
;; user
(setq user-full-name "b"
      frame-title-format '("" "λ bmacs"))

;; initial ui
(when (display-graphic-p)
  (menu-bar-mode 1)
  (tool-bar-mode 0)
  (scroll-bar-mode 0))
(setq inhibit-startup-screen t)

;; custom
(setq custom-file "~/.emacs.d/custom.el")

;; frame size
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; font
(set-face-attribute 'default nil :height 100)

(custom-set-faces
 '(font-lock-string-face ((t (:foreground "#749d76"))))
 '(helm-selection ((t (:background "#333436" :foreground "#76a4cc")))))

;; format
(show-paren-mode 1)

;; generic keybinds
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-c C-k") 'compile)
(global-set-key (kbd "C-x k") 'kill-buffer-and-window)
(global-set-key (kbd "C-x 2") (lambda ()
				(interactive)
				(split-window-vertically)
				(other-window 1)))
(global-set-key (kbd "C-x 3") (lambda ()
				(interactive)
				(split-window-horizontally)
				(other-window 1)))

;; scroll
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))
      mouse-wheel-follow-mouse 't
      scroll-step 1)

;; tooltip
(setq tooltip-use-echo-area t)

;; usability
(setq sentence-end-double-space nil
      select-enable-clipboard t
      max-mini-window-height 10)
(transient-mark-mode t)
(delete-selection-mode t)
(column-number-mode 1)

;; native comp
(setq native-comp-async-report-warnings-errors nil)

;; version control, saves, backups
(make-directory "~/.emacs.d/auto-save" t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save" t))
      backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t
      version-control t
      delete-old-versions t
      kept-new-versions 2
      kept-old-versions 2
      create-lockfiles nil)

;; tramp
(with-eval-after-load 'tramp-cache
    (setq tramp-persistency-file-name "~/.emacs.d/tramp"))
(setq tramp-default-method "ssh"
      tramp-use-ssh-controlmaster-options nil)
(add-to-list 'backup-directory-alist
             (cons tramp-file-name-regexp nil))

;; windmove
(windmove-default-keybindings)

;;; FUNCS
;; initial setup
(defun bmacs-initial-setup ()
  "Initial setup."
  (interactive)
  (all-the-icons-install-fonts 1)
  (nerd-icons-install-fonts 1)
  (yasnippet-snippets-initialize))

(defun bmacs-apply-theme ()
  "Apply theme customizations."
  (interactive)
  (shell-command "sed -i 's/#33CED8/#539296/g; \
s/#33CCDD/#44b9b1/g; s/#EBBF83/#86799c/g; s/#EEBB88/#ecbe7b/g; \
s/#5EC4FF/#6c93a3/g; s/#55CCFF/#51afef/g; s/#70E1E8/#7db7ba/g; \
s/#77EEEE/#46d9ff/g' ~/.emacs.d/elpa/doom-themes-*/doom-city-lights-theme.el")
  (load-theme 'doom-city-lights t))

;; utility
(defun bmacs-format-json ()
  "Json beautify."
  (interactive)
  (let ((start (if mark-active (min (point) (mark)) (point-min)))
        (end (if mark-active (max (point) (mark)) (point-max))))
    (shell-command-on-region start end
       "python3 -m json.tool" (current-buffer) t)))

;; ide
(defun ide-defaults ()
  "Default ide hook."
  (flycheck-mode)
  (display-line-numbers-mode 1)
  (rainbow-delimiters-mode 1)
  (smartparens-mode 1)
  (yas-minor-mode 1))

(defun ide-backends ()
  "Default backends."
  (setq-local company-backends
	      '((company-capf
		 company-dabbrev
		 company-dabbrev-code
		 company-yasnippet))))

;; cfn lint
(defun ide-cfn ()
  "Cfn hook."
  (flycheck-define-checker cfn-lint
    "Install cfn-lint first: pip install cfn-lint"
    :command ("cfn-lint" "-f" "parseable" source)
    :error-patterns ((warning line-start (file-name) ":" line ":" column
			      ":" (one-or-more digit) ":" (one-or-more digit) ":"
			      (id "W" (one-or-more digit)) ":" (message) line-end)
		     (error line-start (file-name) ":" line ":" column
			    ":" (one-or-more digit) ":" (one-or-more digit) ":"
			    (id "E" (one-or-more digit)) ":" (message) line-end))
    :modes (cfn-json-mode cfn-yaml-mode))
  (add-to-list 'flycheck-checkers 'cfn-lint)
  (flycheck mode))

;; before save
(defun ide-before-save ()
  "Before save."
  (when (or (eq major-mode 'c-mode)
            (eq major-mode 'rustic-mode))
    (lsp-format-buffer)))

(add-hook 'before-save-hook 'ide-before-save)

;;; PACKAGES
(setq use-package-always-ensure t)

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(use-package all-the-icons
  :demand t)

(use-package auto-package-update
  :defer t
  :config (setq auto-package-update-delete-old-versions t)
  :hook (auto-package-update-after . bmacs-apply-theme))

(use-package bison-mode
  :defer t
  :mode "\\.y\\'"
  :config
  (setq c-default-style "gnu"
	c-basic-offset 2)
  :hook ((bison-mode . ide-defaults)
	 (bison-mode . ide-backends)))

(use-package centaur-tabs
  :demand t
  :init
  (setq centaur-tabs-height 18
	centaur-tabs-set-icons t
	centaur-tabs-set-modified-marker t)
  (centaur-tabs-mode t)
  :hook (neotree-mode . centaur-tabs-local-mode))

(use-package company
  :demand t
  :config
  (setq company-idle-delay 0.2
	company-minimum-prefix-length 2
	company-selection-wrap-around t)
  (global-company-mode)
  :bind (:map company-active-map
	      ([tab] . company-complete-selection)))

(use-package conda
  :defer t
  :config (conda-env-initialize-interactive-shells)
  :custom (conda-anaconda-home "/home/b/miniconda3"))

(use-package dashboard
  :demand t
  :config
  (setq dashboard-banner-logo-title "λ B's MACroS λ"
	dashboard-startup-banner "~/.emacs.d/bsetup/bmacs-logo.jpeg"
	dashboard-items '((recents  . 5)
			  (bookmarks . 5)
			  (projects . 5)
			  (agenda . 5)
			  (registers . 5))
	dashboard-set-heading-icons t
	dashboard-set-file-icons t
	dashboard-set-init-info t
	dashboard-footer-messages '("Welcome traveler...")
	dashboard-footer-icon (all-the-icons-faicon
			       "shield"
			       :height 1.1
			       :v-adjust -0.05
			       :face 'font-lock-keyword-face))
  (dashboard-open))

(use-package dimmer
  :demand t
  :config
  (setq dimmer-fraction 0.20)
  (dimmer-configure-helm)
  (dimmer-mode t))

(use-package dockerfile-mode
  :defer t
  :mode "\\^[Dd]ockerfile\\'"
  :hook ((dockerfile-mode . ide-defaults)
	 (dockerfile-mode . ide-backends)))

(use-package doom-modeline
  :demand t
  :config
  (setq doom-modeline-height 25
	doom-modeline-bar-width 3
	doom-modeline-project-detection 'project
	doom-modeline-buffer-file-name-style 'truncate-upto-project
	doom-modeline-icon (display-graphic-p)
	doom-modeline-major-mode-icon t
	doom-modeline-major-mode-color-icon t
	doom-modeline-buffer-state-icon t
	doom-modeline-buffer-modification-icon t
	doom-modeline-unicode-fallback nil
	doom-modeline-minor-modes t
	doom-modeline-enable-word-count nil
	doom-modeline-buffer-encoding t
	doom-modeline-indent-info nil
	doom-modeline-checker-simple-format t
	doom-modeline-number-limit 99
	doom-modeline-vcs-max-length 12
	doom-modeline-persp-name t
	doom-modeline-display-default-persp-name nil
	doom-modeline-lsp t
	doom-modeline-evil-state-icon t
	doom-modeline-env-version t
	doom-modeline-env-enable-python t
	doom-modeline-env-load-string "..."
	doom-modeline-before-update-env-hook nil
	doom-modeline-after-update-env-hook nil)
  (doom-modeline-mode 1))

(use-package doom-themes
  :demand t
  :config
  (setq doom-themes-enable-bold t
	doom-themes-enable-italic t
	doom-themes-neotree-file-icons t)
  (doom-themes-visual-bell-config)
  (doom-themes-neotree-config)
  (load-theme 'doom-city-lights t))

(use-package exec-path-from-shell
  :demand t
  :config
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-env "PATH"))

(use-package flycheck
  :defer t)

(use-package flymake-shellcheck
  :defer t)

(use-package helm
  :defer t
  :config
  (setq helm-autoresize-min-height 20
	helm-autoresize-max-height 20
	helm-scroll-amount 4
	helm-split-window-inside-p t
	helm-echo-input-in-header-line t
	helm-ff-file-name-history-use-recentf t
	helm-move-to-line-cycle-in-source t
	helm-mode-fuzzy-match t
	helm-buffers-fuzzy-matching t
	helm-semantic-fuzzy-match t
	helm-M-x-fuzzy-match t
	helm-imenu-fuzzy-match t
	helm-lisp-fuzzy-completion t
	helm-locate-fuzzy-match t
	helm-buffer-skip-remote-checking t
	helm-display-header-line nil)
  (helm-mode 1)
  (helm-autoresize-mode 1)
  :bind (("C-c h" . helm-command-prefix)
	 ("M-x" . helm-M-x)
	 ("M-y" . helm-show-kill-ring)
	 ("C-x b" . helm-buffers-list)
	 ("C-x C-f" . helm-find-files)
	 (:map helm-map
	       ([tab] . helm-execute-persistent-action))))

(use-package helm-descbinds
  :defer t
  :config (helm-descbinds-mode 1))

(use-package helm-tramp
  :defer t)

(use-package helpful
  :defer t
  :bind (("C-h f" . helpful-callable)
	 ("C-h v" . helpful-variable)
	 ("C-h k" . helpful-key)
	 ("C-c C-d" . helpful-at-point)
	 ("C-h F" . helpful-function)
	 ("C-h C" . helpful-command)))

(use-package lsp-mode
  :defer t
  :config
  (setq lsp-eldoc-render-all t
	lsp-idle-delay 0.5
	lsp-completion-provider :none
	lsp-diagnostics-provider :auto
	lsp-pylsp-plugins-pylint-enabled t
	lsp-pylsp-plugins-autopep8-enabled t
	lsp-rust-analyzer-cargo-watch-command "clippy"
	lsp-rust-analyzer-server-display-inlay-hints t
	lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial"
	lsp-rust-analyzer-display-chaining-hints t
	lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil
	lsp-rust-analyzer-display-closure-return-type-hints t
	lsp-rust-analyzer-display-parameter-hints nil
	lsp-rust-analyzer-display-reborrow-hints nil))

(use-package magit
  :defer t
  :bind ("C-x g" . magit-status))

(use-package neotree
  :defer t
  :bind ([f8] . neotree-toggle)
  :config (setq neo-window-width 30))

(use-package nginx-mode
  :defer t
  :config
  :hook ((nginx-mode . ide-defaults)
	 (nginx-mode . ide-backends)))

(use-package paredit
  :defer t)

(use-package projectile
  :demand t
  :config (projectile-mode 1))

(use-package py-autopep8
  :defer t
  :config (setq py-autopep8-options '("--max-line-length=79")))

(use-package pyvenv
  :demand t
  :config (pyvenv-activate "~/code/python/venvs/default"))

(use-package rainbow-delimiters
  :defer t)

(use-package smartparens
  :defer t
  :config
  (sp-with-modes '(rustic-mode)
    (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))))

(use-package undo-tree
  :demand t
  :config
  (setq undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo")))
  (global-undo-tree-mode))

(use-package visual-regexp
  :defer t
  :bind (("C-c r" . vr/replace)
	 ("C-c q" . vr/query-replace)
	 ("C-r" . vr/isearch-backward)
	 ("C-s" . vr/isearch-forward)))

(use-package visual-regexp-steroids
  :defer t)

(use-package vterm
  :defer t
  :config (setq vterm-max-scrollback 5000))

(use-package web-mode
  :defer t
  :mode "\\.html\\'"
  :config (setq web-mode-markup-indent-offset 2)
  :hook ((web-mode . ide-defaults)
	 (web-mode . ide-backends)))

(use-package x86-lookup
  :defer t
  :config (setq x86-lookup-pdf "~/.emacs.d/intel64_ia32_architecture.pdf"))

(use-package yaml-mode
  :defer t
  :mode ("\\.yml\\'" "\\.yaml\\'")
  :hook ((yaml-mode . ide-defaults)
	 (yaml-mode . ide-backends)))

(use-package yasnippet-snippets
  :defer t)

;;; IDE
;; asm
(use-package asm-mode
  :ensure nil
  :defer t
  :mode "\\.[Ss]\\'"
  :config
  (setq tab-width 2
	indent-line-function 'insert-tab
	tab-stop-list (number-sequence 2 60 2))
  :bind ("C-h x" . x86-lookup)
  :hook ((asm-mode . ide-defaults)
	 (asm-mode . ide-backends)))

;; awk
(use-package awk-mode
  :ensure nil
  :defer t
  :mode "\\.awk\\'"
  :hook ((awk-mode . ide-defaults)
	 (awk-mode . ide-backends)))

;; c
(use-package c-mode
  :ensure nil
  :defer t
  :mode "\\.cu?\\'"
  :config
  (setq c-default-style "gnu"
	c-basic-offset 2)
  :hook ((c-mode . ide-defaults)
	 (c-mode . ide-backends)
	 (c-mode . lsp)))

;; cfn
(define-derived-mode cfn-json-mode js-mode
  "Cfn json."
  (setq js-indent-level 2))

(define-derived-mode cfn-yaml-mode yaml-mode
  "Cfn yaml.")

(add-hook 'cfn-json-mode-hook 'ide-cfn)
(add-hook 'cfn-yaml-mode-hook 'ide-cfn)

;; conf
(use-package conf-unix-mode
  :ensure nil
  :defer t
  :mode "\\.conf\\'"
  :config (setq tab-width 2)
  :hook ((conf-unix-mode . ide-defaults)
	 (conf-unix-mode . ide-backends)))

;; elisp
(use-package emacs-lisp-mode
  :ensure nil
  :defer t
  :mode "\\.el\\'"
  :hook ((emacs-lisp-mode . ide-defaults)
	 (emacs-lisp-mode . ide-backends)))

;; flex
(use-package flex-mode
  :ensure nil
  :defer t
  :mode ("\\.l\\'" "\\.lex\\'")
  :config
  (setq c-default-style "gnu"
	c-basic-offset 2)
  :hook ((flex-mode . ide-defaults)
	 (flex-mode . ide-backends)))

;; json
(use-package json-mode
  :ensure nil
  :defer t
  :mode "\\.json\\'"
  :hook ((json-mode . ide-defaults)
	 (json-mode . ide-backends)))

;; lisp interaction
(use-package lisp-interaction-mode
  :ensure nil
  :defer t
  :hook ((lisp-interaction-mode . ide-defaults)
	 (lisp-interaction-mode . ide-backends)))

;; markdown
(use-package markdown-mode
  :ensure nil
  :defer t
  :mode "\\.md\\'"
  :hook ((markdown-mode . ide-defaults)
	 (markdown-mode . ide-backends)))

;; python
(use-package python-mode
  :ensure nil
  :defer t
  :mode "\\.py\\'"
  :hook ((python-mode . lsp))
	 (python-mode . ide-defaults)
	 (python-mode . ide-backends)
	 (python-mode . py-autopep8-mode))

;; rust
(use-package rustic
  :defer t
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status)
              ("C-c C-c e" . lsp-rust-analyzer-expand-macro)
              ("C-c C-c d" . dap-hydra)
              ("C-c C-c h" . lsp-ui-doc-glance))
  :hook ((rustic-mode . ide-defaults)
	 (rustic-mode . ide-backends)))

;; sh
(use-package sh-mode
  :ensure nil
  :defer t
  :mode "\\.sh\\'"
  :config
  (flymake-mode)
  (flymake-shellcheck-load)
  :hook ((sh-mode . ide-defaults)
	 (sh-mode . ide-backends)
	 (sh-mode . (lambda ()
                     (setq-local indent-tabs-mode nil
				 sh-basic-offset 4)))))

;; sql
(use-package sqlformat
  :defer t
  :config
  (setq sqlformat-command 'pgformatter
	sqlformat-args '("-s2" "-w79" "-U2")))
(use-package sql-mode
  :ensure nil
  :defer t
  :mode "\\.sql\\'"
  :hook ((sql-mode . ide-defaults)
	 (sql-mode . ide-backends)
	 (sql-mode . sqlformat-on-save-mode)))

;; zig
(use-package reformatter
  :defer t)
(use-package zig-mode
  :defer t
  :mode "\\.zig\\'"
  :hook ((zig-mode . lsp))
	 (zig-mode . ide-defaults)
	 (zig-mode . ide-backends))

;;; LLM
;; gptel
(use-package gptel
  :defer t
  :config
  (setq gptel-model 'mistral:latest
	gptel-backend (gptel-make-ollama "mistral"
			:host "localhost:11434"
			:stream t
			:models '(mistral:latest)))
  (gptel-make-ollama "qwen"
    :host "localhost:11434"
    :stream t
    :models '(qwen2.5-coder:14b))
  (gptel-make-ollama "deepseek"
    :host "localhost:11434"
    :stream t
    :models '(deepseek-r1:14b)))

;;; init.el ends here
