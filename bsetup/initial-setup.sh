#!/bin/bash

sudo apt install -y emacs emacs-common-non-dfsg fonts-ibm-plex python3-venv \
     clang libclang-dev libtool libvterm-dev

python3 -m venv ~/code/python/venvs/default
. ~/code/python/venvs/default/bin/activate
pip install --upgrade pip setuptools wheel
pip install python-lsp-server pylint autopep8
